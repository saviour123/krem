using System;
using System.Data;
using bloggit.Model;
using Npgsql;

namespace bloggit.Repository
{
    public class letsBlogRepository
    {
        private string connectionString;
        public letsBlogRepository()
        {
            var username = Environment.GetEnvironmentVariable("USER");
            var pass = Environment.GetEnvironmentVariable("PASSWORD");
            connectionString = $"Host=localhost;Username={username};Password={pass};Database=sav";
        }


        internal IDbConnection Connection 
        {
            get {
                return new NpgsqlConnection(connectionString);
            }
        }

        #region CRUD
        public void Add(Bloggit item)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = (NpgsqlConnection) conn;
                    cmd.CommandText = "INSERT INTO blog (publish_at, story) VALUES (@publish_at, @story)";
                    cmd.Parameters.AddWithValue("publish_at", item.PublishedAt);
                    cmd.Parameters.AddWithValue("story", item.Story);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion CRUD

    }
}