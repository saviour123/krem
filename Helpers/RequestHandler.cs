using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using bloggit.Model;
using Flurl;
using Flurl.Http;
using Polly;

namespace bloggit.Helpers
{
    public class RequestHandler
    {
        public string RequestNews(string source)
        {
            var httpClient = new HttpClient();
            var statuscode = "";            
            
            var maxRetryAttempt = 3;
            var pauseBetweenFailures = TimeSpan.FromSeconds(2);

            var retryPolicy = Policy
                .Handle<HttpRequestException>()
                .WaitAndRetryAsync(maxRetryAttempt, i => pauseBetweenFailures);
            
            var key = Environment.GetEnvironmentVariable("newskey");
            
            retryPolicy.ExecuteAsync(async () => {
                var response = await httpClient.GetAsync($"https://newsapi.org/v1/articles?source=talksport&apiKey={key}");
                statuscode = response.EnsureSuccessStatusCode().ToString();
            });

            return statuscode;
        }
    }        
}