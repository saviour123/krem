using System;
using MediatR;

namespace bloggit.Helpers
{
    public class MBlog : IRequest<bool>
    {
        public string title { get; set; }
        public string Story { get; set; }
        public DateTimeOffset PublishedAt { get; set; }
    }
}