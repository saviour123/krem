﻿
using System;
using System.Threading;
using System.Threading.Tasks;
using bloggit.Model;
using MediatR;

namespace bloggit.Helpers
{
    public class AddBlogHandler : IRequestHandler<MBlog, bool>
    {
        private readonly BlogContext _dbcontext;

        public AddBlogHandler(BlogContext context)
        {
            _dbcontext = context;
        }

        public async Task<bool> Handle(MBlog request, CancellationToken cancellationToken)
        {
            _dbcontext.Blog.Add(new Bloggit { PublishedAt = DateTimeOffset.Now, Story= request.Story, title = request.title});
            _dbcontext.SaveChanges();
            return true;
        }

    } 
}