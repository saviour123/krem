﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using bloggit.Model;
using bloggit.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace bloggit.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class BloggitController : Controller
    {

        private readonly BlogContext _dbcontext;
        private readonly IMediator _mediator;

        public BloggitController(BlogContext context, IMediator mediator)
        {
            _dbcontext = context;
            _mediator = mediator;
        }

        // GET api/bloggit
        [HttpGet, Route("all")]
        public List<Bloggit> GetAll()
        {
            return _dbcontext.Blog.ToList();
        }

        // GET api/bloggit/2
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var result  = _dbcontext.Blog.FirstOrDefault(m => m.Id == id);
            return new ObjectResult(result);
        }

        // POST api/bloggit
        [HttpPost]
        public Dictionary<string,string> Post([FromBody]MBlog blog)
        {
            Dictionary<string, string> response = new Dictionary<string, string>();
            MBlog item = new MBlog { title= blog.title, Story=blog.Story, PublishedAt=DateTimeOffset.Now};

            if (ModelState.IsValid)
            {
                response.Add("status", "failure");
                return response;
            }

            var hello = _mediator.Send(blog);
            
            response.Add("status", "failure");
            return response;
        }


        // PUT api/bloggit/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody]MBlog blog)
        {
            // implement id validation
            var post  = _dbcontext.Blog.FirstOrDefault(m => m.Id == id);
            post.Story = blog.Story;
            post.title = blog.title;
            _dbcontext.SaveChanges();
            return new ObjectResult("success");
        }

        // DELETE api/bloggit/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var post  = _dbcontext.Blog.FirstOrDefault(m => m.Id == id);
            _dbcontext.Blog.Remove(post);
            return new ObjectResult("success");
        }

        // GET api/bloggit/sportsnews
        [HttpGet, Route("sportsnews")]
        public ActionResult GetNews()
        {
            var main = new RequestHandler();
            var test = main.RequestNews("talksport");
            return new ObjectResult(test);
        }
    }
}
