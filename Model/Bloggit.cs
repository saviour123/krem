using System;
using System.ComponentModel.DataAnnotations;

namespace bloggit.Model
{
    public class Bloggit
    {
        [Key]
        public int Id{ get; set; }
        [Required]
        public string title { get; set; }
        public string Story { get; set; }
        public DateTimeOffset PublishedAt { get; set; }
    }
}