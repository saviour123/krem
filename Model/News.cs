using System.Collections.Generic;

namespace bloggit.Model
{
    public class SportsNews
    {
        public string status { get; set; }
        public string source { get; set; }
        public string sortby { get; set; }
        public List<article> article { get; set; }
    }

    public class article
    {
        public string author { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public string urltoImage { get; set; }
        public string publishAt { get; set; }
    }
}