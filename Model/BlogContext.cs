using Microsoft.EntityFrameworkCore;

namespace bloggit.Model
{
    public class BlogContext : DbContext
    {
        public BlogContext(DbContextOptions<BlogContext> options) : base(options) 
        {
            //
        }

        public DbSet<bloggit.Model.Bloggit> Blog {get; set;}
    }  
}